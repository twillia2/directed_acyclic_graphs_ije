Below we (Luigi Gagliardi,Thomas Christie Williams) present some tips on how to use the Dagitty package to draw and interpret Directed Acyclic Graphs (DAGs), using data from a study by Pape et al published in the International Journal of Epidemiology. 

For more extensive notes on the use of DAGs, please refer to our publication in Pediatric Research "Directed acyclic graphs: a tool for causal studies in paediatrics." Pediatr Res. 2018 Oct 4;84(4):487–493 https://doi.org/10.1038/s41390-018-0071-3

To begin, copy the code in this directory ("DAG_Pape_et_al_IJE_2020") and open Dagitty [http://www.dagitty.net/dags.html]

Paste the code into the “model Code” panel (middle-bottom right)

Click Update DAG (The DAG will appear)

Color-coding of variables:
* Green with a triangle: exposure
* Blue with an “I”: outcome
* Other variables: green, blue, red, gray
* Variables adjusted on: white


Causal effects (direct and indirect) are represented by green arrows [“edges”]; biasing paths by red arrows, and other,non-biasing paths (closed paths), by black arrows.

Dagitty displays the “Causal effect identification” in the top right pane, reporting which adjustments are necessary to obtain total effect estimation. The model displayed by the authors in Supplementary figure 1 – where neither “Grandmother’s smoke during pregnancy”, nor “Parent’s smoke status” are adjusted on – allows the identification of the total effect when adjusting for the “Minimal sufficient adjustment sets” (all other variables, as done in the paper).
There are 3 green paths, conveying causal effects.

By adjusting for parent’s smoke status (move the mouse over the variable and hit the “a” key of the keyboard: the variable turns white), the program warns that “The total effect cannot be estimated due to adjustment for an intermediate or a descendant of an intermediate”.

Some of the green paths are now black or red. No further adjustment can restore the total effect estimation as before.

